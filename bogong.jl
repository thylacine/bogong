#=
Bogong: Pipe for compositions for the Bogong Centre for Sound Culture, 2022–23 ??

Working title: Lying Flat

Josh Berson, josh@joshberson.net
August 2021
December 2020
August 2019

CC BY-NC-SA 4.0
… Of course feel free, but please credit if you borrow or adapt in a more or less intact form

Further experiments in permutational recomposition,
ramping shingled field recordings by spectral flatness and zero-crossing rate

An exploration of latent perceptual pattern spaces for timbre.

Compare Esling et al 2018: https://acids-ircam.github.io/variational-timbre/

  Audio descriptors seem to be organized in a non-linear way across our spaces.
  However, they still exhibit both locally smooth evolution and an overall
  logical organization. This shows that our model is able to organize audio 
  variations. A very interesting observation comes from the topology of the 
  centroid. Indeed, all perceptual studies underline its linear correlation 
  to timbre perception, which is partly confirmed by our model (see Figure 4). 
  This confirms the perceptual relevance of these latent spaces. However, 
  this also shows that the relation between centroid and timbre perception 
  might not be entirely linear.

Esling et al’s approach:

  1. They start with perceptual dissimilarity ratings
  2. Use MDS to generate a timbre space
  3. Use PCA to project into three dimensions
  4. Extract centroid, flatness, bandwidth, etc for 2d slices from the PCA projection

See also: https://aqibsaeed.github.io/2016-09-03-urban-sound-classification-part-1/

TODO:
– Add MFCC, PCA
– A single output: PCA of first four central moments of f, f', f'' of first 13 MFCCs + flux, flatness, zcr
=#

using StatsBase, MultivariateStats, WAV, DSP, MFCC #, Plots, ColorSchemes


function shingle( path, len, step )
  #=
  Read an audio file, construct an array of shingles
  Returns shingles, sample rate, bitrate, number of channels
  =#

  s, fs, bitrate = wavread(path) # NB, no bandpass filtering this time

  nsamples, nchannels = size(s)

  # Seconds to frames
  len = Int64(floor(len * fs))
  step = Int64(floor(step * fs))
  step = min(step, len - 1)
  overlap = len - step

  # Snip s so it contains a whole number of shingles
  # to keep channels aligned when we reshape
  s = s[1 : overlap * div(nsamples, overlap), : ]

  # Reshape s into Array{Float64,1}, channels in series
  s = reshape(s, prod(size(s)))

  # Virtual shingling with DSP.Periodograms.ArraySplit — no malloc
  shingles = arraysplit(s, len, overlap, len #=nfft=#, tukey(len, .01) )
    # Window to reduce Gibbs popping
    # Hann window yields Doppler-type artifact
    # Tukey window with steep rolloff does better
    # But does it improve the outcome?
    # TODO. Listen with multiple recordings, with and without Tukey window at .01, .05

  shingles, fs, bitrate, nchannels
end

function extract( s, fs )
  #=
  Extract features from a shingle
  =#

  # Zero-crossing rate. /2 bc sign is in [-1, 1], so diff will be in [-2, 0, 2]
  zcr = mean(abs.(diff(sign.(s)))) / 2.

  # (mfcc, pspec, mfcc_param_dict) = mfcc(s, sr=fs)
  (mfcc, audio_meta_dict, mfcc_params_dict)
    = feacalc(s; sr=fs, chan=0, sadtype=:none, normtype=:none, defaults=:wbspeaker, augtype=:ddelta)
    # augtype=:ddelta gives first and second derivatives alongside MFCCs

  mfcc_cols = [ view(mfcc,:,c) for c in 1:size(mfcc,2) ]

  mfµ = mean.(mfcc_col)
  mfσ2 = var.(mfcc_cols)
  mfγ = skewness.(mfcc_cols)
  mfkur = kurtosis.(mfcc_cols)

  # Normalized power spectrum
  # From https://docs.juliadsp.org/stable/periodograms/#DSP.Periodograms.power
  #   For a Spectrogram, returns the computed power at each frequency and time bin
  #   as a Matrix. Dimensions are frequency × time.
  # I.e., each row is a time-domain FFT frame
  # For spectrogram(), default is frame length n=div(length(s) / 8), step=div(n / 2)
  # so 2n - 1 = 15 bins
  # Try n=div(length(s) / 16) for higher resolution in the f-domain
  psdmat = power(spectrogram(s; fs=fs, window=hamming))

    # TODO. Alas, we compute power spectrum twice —
    # using MFCC.feacalc() for derivatives, which renders the underlying pspec inaccessible
    # So at the moment we use feacalc() and recompute the spectrogram here
    # Could use mfcc() instead and deltas() for derivatives
    # But at least this way we have control over windowing for the psd used for flatness and flux
      # TODO. Does mfcc() return the rv of DSP.spectrogram() or power(spectrogram(s …)) ?
      # https://juliapackages.com/p/mfcc: “… the power spectrum computed with DSP.spectrogram() …”

  psdvec = [ psdmat[i,:] for i in 1 : size(psdmat, 1) ]
    # https://stackoverflow.com/questions/38833324/how-to-convert-a-matrix-into-a-vector-of-vectors
    # Each element of psdvec holds one row of psdmat,
    # i.e., the power spectral density for one FFT frame

  centroid = mean(meanfreq.(psdvec, fs))
  flatness = mean(geomean.(psdvec) ./ mean.(psdvec))
    # https://dsp.stackexchange.com/questions/2045/how-do-you-calculate-spectral-flatness-from-an-fft

  # Spectral flux (RMS version)
  delta = diff(psdmat; dims=1)
  flux = sqrt(sum(delta .^ 2)) / size(psdmat, 1)

  # TODO. Weight flux, flatness, and MFCCs,
  # combine into a vector, distill with PCA, take the first principal dimension?
  # https://github.com/JuliaStats/MultivariateStats.jl/blob/master/docs/src/pca.md
  # perhaps do 2 ramps -- first 2 PC

  (
    zcr = zcr,
    centroid = centroid,
    flatness = flatness,
    flux = flux,
    mfcc = [ mfµ; mfσ2; mfγ; mfkur ]
  )
end

function gen( src; len = .75, step = .375,
              path = "/Users/josh/Projects/bogong/audio",
              compose = true )
  shingles, fs, bitrate, nchannels = shingle("$path/$src.wav", len, step)
    # Within shingles, channels come in series
    # Thus, ...chan 1–shingle 1...chan 1–shingle 2...chan 1–shingle n...chan 2–shingle 1...

  features = extract.(shingles, fs)

  #
  # Iterate features to generate permutation compositions

  nshingles = div(length(shingles), nchannels)

  # Extract the MFCC feature space for the first channel
  mfcc_space = [ features[i][mfcc] for i in 1:nshingles ]

  # PCA
  mfcc_pca = fit(PCA, mfcc_space; maxoutdim=2)
  print(eigvecs(mfcc_pca))
  print(principalvars(mfcc_pca))

  #  with scalars for the first two principal components

  # [ [(feature)+]+ ]
  # each tuple is a set of features for a single channel of a single shingle
  # each inner array is the feature tuple for the channels of a single shingle
  # the outer array is the series of shingles
  bychannel = [ [ features[i * c] for c in 1:nchannels ] for i in 1:nshingles ]

  featureids = propertynames(features[1])

  # Permute, averaging per-shingle feature values across channels (no need to divide by nchannels)
  permuts = [ ( id = f,
                permut = sortperm(bychannel; by = s -> sum([ channel[f] for channel in s ]), rev=true) )
              for f in featureids ]

  # Merge channels, concatenate, write
  if compose
    lenms = Int64(round(len * 1000))
    stepms = Int64(round(step * 1000))
    nsamples = Int64(len * fs) # samples/shingle
    buf = zeros(nshingles * nsamples, nchannels)
    for f in permuts
     for i in 1:nshingles
        for c in 1:nchannels
          buf[(i - 1) * nsamples + 1 : i * nsamples, c] = shingles[f.permut[i] * c]
        end
      end
      wavwrite( buf, "$path/out/$src $(f.id) $(lenms)⁄$stepms.wav";
                Fs=fs, nbits=bitrate, compression=WAVE_FORMAT_PCM )
    end
  end
end

function multi( src )
  param = map.( (len, stepratio) -> (len = len, step = stepratio * len),
                [ .6, .75, ],
                transpose([ 1. /3., .5, ]) )

  for p in param
    gen(src; len = p.len, step = p.step)
  end
end
