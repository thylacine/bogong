#=
M2: Further experiments in audio feature manifolds

Josh Berson, josh@joshberson.net
July 2023

CC BY-NC-SA 4.0
… Of course feel free, but please credit if you borrow or adapt in a more or less intact form

Further experiments in permutational recomposition,
ramping shingled field recordings by the principal components
of a projected high-dim feature space

An exploration of latent perceptual pattern spaces for timbre.

Compare Esling et al 2018: https://acids-ircam.github.io/variational-timbre/

  Audio descriptors seem to be organized in a non-linear way across our spaces.
  However, they still exhibit both locally smooth evolution and an overall
  logical organization. This shows that our model is able to organize audio 
  variations. A very interesting observation comes from the topology of the 
  centroid. Indeed, all perceptual studies underline its linear correlation 
  to timbre perception, which is partly confirmed by our model (see Figure 4). 
  This confirms the perceptual relevance of these latent spaces. However, 
  this also shows that the relation between centroid and timbre perception 
  might not be entirely linear.

Esling et al’s approach:

  1. They start with perceptual dissimilarity ratings
  2. Use MDS to generate a timbre space
  3. Use PCA to project into three dimensions
  4. Extract centroid, flatness, bandwidth, etc for 2d slices from the PCA projection

See also: https://aqibsaeed.github.io/2016-09-03-urban-sound-classification-part-1/

=#

using StatsBase, MultivariateStats, ManifoldLearning
using WAV, DSP
using Makie #, CairoMakie, TopoPlots


function shingle( path, len, step )
  #=
  Read an audio file, construct an array of shingles
  Returns shingles, sample rate, bitrate, number of channels
  =#

  s, fs, bitrate = wavread(path) # NB, no bandpass filtering this time

  nsamples, nchannels = size(s)

  # Seconds to frames
  len = Int64(floor(len * fs))
  step = Int64(floor(step * fs))
  step = min(step, len - 1)
  overlap = len - step

  # Snip s so it contains a whole number of shingles
  # to keep channels aligned when we reshape
  s = s[1 : overlap * div(nsamples, overlap), : ]

  # Reshape s into Array{Float32,1}, channels in series
  s = reshape(s, prod(size(s)))

  # Virtual shingling with DSP.Periodograms.ArraySplit — no malloc
  shingles = arraysplit(s, len, overlap, len #=nfft=#, tukey(len, .01) )
    # Window to reduce Gibbs popping
    # Hann window yields Doppler-type artifact
    # Tukey window with steep rolloff does better
    # But does it improve the outcome?
    # TODO. Listen with multiple recordings, with and without Tukey window at .01, .05

  shingles, fs, bitrate, nchannels
end

const global LOG_ENERGY = 1
const global CENTROID = 2
const global ZCR = 3
const global FLATNESS = 4
const global FLUX = 5
function extract( s, fs )
  #=
  Extract features from a shingle
  =#

  # Log energy
  logenergy = log10(1 + sum(s .* s) / length(s)) #max(size(s)))

  # Zero-crossing rate. /2 bc sign is in [-1, 1], so diff will be in [-2, 0, 2]
  zcr = mean(abs.(diff(sign.(s)))) / 2.

  #= (mfcc, pspec, mfcc_param_dict) = mfcc(s, sr=fs)
  (mfcc, audio_meta_dict, mfcc_params_dict) =
    feacalc(s; sr=fs, chan=0, sadtype=:none, normtype=:none, defaults=:wbspeaker, augtype=:ddelta)
    # augtype=:ddelta gives first and second derivatives alongside MFCCs

  # Each element of mfcc_view contains the mfcc data for a single feature
  # (log energy + 19 MFCCs + d1 and d2 of same, thus length(mfcc_cols) == 60)
  # with each element holding values for each sample frame of the MFCC
  mfcc_view = [ view(mfcc,:,c) for c in 1:size(mfcc,2) ]

  mfµ = mean.(mfcc_view)
  mfσ2 = var.(mfcc_view)
  mfγ = skewness.(mfcc_view)
  mfkur = kurtosis.(mfcc_view)
  =#

  # Normalized power spectrum
  # From https://docs.juliadsp.org/stable/periodograms/#DSP.Periodograms.power
  #   For a Spectrogram, returns the computed power at each frequency and time bin
  #   as a Matrix. Dimensions are frequency × time.
  # I.e., each row is a time-domain FFT frame
  # For spectrogram(), default is frame length n=div(length(s) / 8), step=div(n / 2)
  # so 2n - 1 = 15 bins
  # Try n=div(length(s) / 16) for higher resolution in the f-domain
  psdmat = power(spectrogram(s; fs=fs, window=hamming))

  psdvec = [ psdmat[i,:] for i in 1 : size(psdmat, 1) ]
    # https://stackoverflow.com/questions/38833324/how-to-convert-a-matrix-into-a-vector-of-vectors
    # Each element of psdvec holds one row of psdmat,
    # i.e., the power spectral density for one FFT frame

  centroid = mean(meanfreq.(psdvec, fs))
  flatness = mean(geomean.(psdvec) ./ mean.(psdvec))
    # https://dsp.stackexchange.com/questions/2045/how-do-you-calculate-spectral-flatness-from-an-fft

  # Spectral flux (RMS version)
  delta = diff(psdmat; dims=1)
  flux = sqrt(sum(delta .^ 2)) / size(psdmat, 1)

  [ 
    logenergy ;
    centroid ;
    zcr ;
    flatness ;
    flux
    # mfµ ; mfσ2 ; mfγ ; mfkur
  ]
end

function gen( src; len = .5, step = .25,
              path = "/Users/josh/Projects/bogong/audio",
              compose = true )
  shingles, fs, bitrate, nchannels = shingle("$path/$src.wav", len, step)
    # In shingles, channels appear in series
    # Thus, ...chan 1–shingle 1...chan 1–shingle n...chan 2–shingle 1...

  nshingles = div(length(shingles), nchannels)

  # Extract features for the first channel
  # TODO: Why do extract.(shingles, fs) and map(extract(...)) not work?
  # Both return a vector filled with the feature vector for the final frame
  features = [ extract(shingles[s], fs) for s in 1:nshingles ]

  # Reshape features into a matrix, one observation (shingle) per column
  # https://discourse.julialang.org/t/how-to-convert-vector-of-vectors-to-matrix/72609
  # https://github.com/JuliaLang/julia/issues/34995
  fmat = convert( AbstractMatrix{Float32}, reduce(hcat, features)' )'

  proj = Dict()
  permut_proj = Dict()

  # PCA
  #model = fit(PCA, fmat; maxoutdim=3)
  #push!(proj, "PCA" => predict(model, fmat))
  #push!(permut_proj, "pca" => predict(model, fmat)[1,:])

  # Multidimensional scaling
  #model = fit(MDS, fmat; distances=false, maxoutdim=2)
  #push!(proj, "mds" => predict(model))
  #push!(permut_proj, "mds" => predict(model)[1,:])

  model = fit(HLLE, fmat; k=12, maxoutdim=2)
  reduced = predict(model)
  push!(proj, "hlle" => reduced)
  push!(permut_proj, "hlle" => reduced[1,:])

  # Permute
  permuts = [ ( id = k, permut = sortperm(permut_proj[k]; rev=true) )
              for k in keys(permut_proj) ]

  figs = Dict()
  for k in keys(proj)
    p = proj[k]

    #fig = Figure(; resolution=(1200,1200))

    logen = fmat[LOG_ENERGY,:]
    fig = scatter(p[1,:], p[2,:])
    #topoplot(fig, logen, p; contours=(color=:white, linewidth=2), label_scatter=true)
    #=topoplot(fig, logen, centroid_flatness;
      contours=(color=:white, linewidth=2),
      labe_scatter=true,
      markersize=5,
      #colormap=:isoluminant_cgo_80_c38_n256, #:viridis
      #colorrange=automatic
      interpolation=CloughTochter(), # SplineInterpolator
      bounding_geometry=Rect(0,0,1200,1200),
      label_text=false
    )=#

    figs[k] = fig
  end
  
  # Merge channels, concatenate, write
  if compose
    lenms = Int64(round(len * 1000))
    stepms = Int64(round(step * 1000))
    nsamples = Int64(len * fs) # samples/shingle
    buf = zeros(nshingles * nsamples, nchannels)
    for f in permuts
     for i in 1:nshingles
        for c in 1:nchannels
          buf[(i - 1) * nsamples + 1 : i * nsamples, c] = shingles[f.permut[i] * c]
        end
      end
      wavwrite( buf, "$path/out/$src-$(f.id)-$(lenms)-$stepms.wav";
                Fs=fs, nbits=bitrate, compression=WAVE_FORMAT_PCM )
      save("$path/out/$src-$(f.id)-$(lenms)-$stepms.png", figs[f.id])
    end
  end
end

function multi( src )
  param = map.( (len, stepratio) -> (len = len, step = stepratio * len),
                [ .6, .75, ],
                transpose([ 1. /3., .5, ]) )

  for p in param
    gen(src; len = p.len, step = p.step)
  end
end
